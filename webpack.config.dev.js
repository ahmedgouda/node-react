import path from 'path'
import webpack from "webpack";

export default {
  entry: [
    'webpack-hot-middleware/client',
    path.resolve(__dirname, 'client/index.js')
  ],
  output: {
    path: '/',
    filename: 'bundle.js',
    publicPath:'/'
  },
  plugins:[
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()    
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'client'),
          path.join(__dirname, 'server/shared'),
      ],
        loaders: [  'babel-loader' ]
      }
    ]
  },
  resolve: {
    
  extensions: ['.ts', '.tsx', '.js'],
  }
}
