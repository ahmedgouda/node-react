import express from "express";
import validateInput from "../shared/validations/signup";

let router = express.Router();



router.post('/',(req,res)=>{
  const { errors , isValid } = validateInput(req.body);

  if(isValid){
    res.json({errors,success: true});
  }else {
    res.status(200).json(errors);
  }
});
export default router;