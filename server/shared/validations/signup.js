import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data){
  let errors = {};
  
  if(Validator.isEmpty(data.username)){
      errors.username = "Username is Required";
  }
  if(!Validator.isEmail(data.email)){
      errors.email = "Email is Invalid";
  }
  if(Validator.isEmpty(data.password)){
      errors.password = "password is Required";
  }
  if(Validator.isEmpty(data.passwordConfirmation)){
      errors.passwordConfirmation = "password confirmation is Required";
  }
  if(!Validator.equals(data.password,data.passwordConfirmation)){
      errors.passwordConfirmation = "password doesn't match";
  }
  if(Validator.isEmpty(data.timezone)){
      errors.timezone = "timezone is Required";
  }

  return{
  errors,
  isValid: isEmpty(errors),

  }
}
