import React from "react";
import { render } from "react-dom";
import { BrowserRouter  , Switch} from 'react-router-dom'
import { Route ,Router, IndexRoute} from "react-router";
import routes from "./routes";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware,compose } from "redux"; 
import  App  from "./component/App";
import  Greetings from "./component/Greetings";
import  SignupPage from "./component/signup/SignupPage";
import rootReducer from './rootReducer';

const store = createStore(
    rootReducer,
    compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
    )
);
render(
    <Provider store={store}>
  <div>
<BrowserRouter>

       <Route  path="/" exact strict render={props =>
  <div className="container">
    <App />
    <Greetings />
  </div>
} />

      
</BrowserRouter>

<BrowserRouter>
  <Route  path="/signup" exact strict render={props =>
  <div className="container">
    <App />
    <SignupPage />
  </div>
} />
        </BrowserRouter>
        </div>
        </Provider>
, document.getElementById('app'));