import React from 'react';
import timezones from '../../data/timezones';
import map from 'lodash/map';
import PropTypes from 'prop-types'
import classnames from 'classnames';
import validateInput from '../../../server/shared/validations/signup';
import TextFieldGroup from '../common/TextFieldGroup';


class SignupForm extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      username : "",
      email : "",
      password : "",
      passwordConfirmation : "",
      timezone : "",
      errors : {},
      isLoading: false
    }

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({
      [e.target.name]: e.target.value
    });
    
  }

  isValid(){
    const {errors ,isValid } = validateInput(this.state);
   
    if(!isValid){
      this.setState({errors})
    }
    return isValid;
  }
  onSubmit(e){
    e.preventDefault();

    if(!this.isValid()){
    this.setState({errors: {},isLoading:true});
    this.props.userSignupRequest(this.state).then(
      ()=>{
        this.props.addFlashMessage({
          type: 'success',
          test: 'you signed up successfullt. Welcome'
        });
        window.location.href = "/";
        // this.context.router.history.push('/');
      }
      ,data => { 
                  
        this.setState({errors: data.data, isLoading:false})
    });
          }
      
        }
  
     
  render() {

    const errors = this.state.errors;
    const options = map(timezones ,(val,key)=>
    <option key={val} value={val}> {key} </option>
    );
    return (
      <form onSubmit={this.onSubmit}>
        <h1>Register Now!</h1>

        <div>
        <TextFieldGroup error={errors.username}
        label="Username"
        onChange={this.onChange}
        value={this.state.username}
        field="username"
        />
        <TextFieldGroup error={errors.email}
        label="Email"
        onChange={this.onChange}
        value={this.state.email}
        field="email"
        />
        <TextFieldGroup error={errors.password}
        label="Password"
        onChange={this.onChange}
        value={this.state.password}
        field="password"
        type="password"
        />
        <TextFieldGroup error={errors.passwordConfirmation}
        label="password confirmation"
        onChange={this.onChange}
        type="password"
        value={this.state.passwordConfirmation}
        field="passwordConfirmation"
        />
          <div className={classnames("form-group",{'has-error':errors.timezone})}>
            <label className="control-label"> Timezones </label>
            <select
              value = {this.state.timezone}
              onChange={this.onChange}
              name="timezone"
              className="form-control"
            >

            <option value="" >Choose your time </option>
            {options}
            </select>
            
            { errors.timezone && <span className="help-block">{errors.timezone}</span>}
          </div>

        </div>
        



        <div className="form-group">
          <button disabled={this.state.isLoading} className="btn btn-primary btn-lg">
            Sign up
          </button>
        </div>
      </form>
    );
  }
}


SignupForm.propTypes = {
  userSignupRequest: PropTypes.func.isRequired,
  addFlashMessage: PropTypes.func.isRequired
}

SignupForm.contextTypes = {
  router: PropTypes.object
}
export default SignupForm;
