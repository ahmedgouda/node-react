import React from 'react';
import { Link } from 'react-router';
// import { connect } from 'react-redux';
// import { logout } from '../actions/authActions';

class NavigationBar extends React.Component {
 
  render() {

    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <a  href="/" className="navbar-brand">Node React</a>
          </div>

          <div className="collapse navbar-collapse">
          <ul className="nav navbar-nav navbar-right">
            <li><a href="signup">Sign up</a></li>
           </ul>
          </div>
        </div>
      </nav>
    );
  }
}



export default NavigationBar;
